<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddConvertionDurationFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('movie_convertion_progress', function (Blueprint $table) {
            $table->timestamp('started')->nullable();
            $table->timestamp('finished')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('movie_convertion_progress', function (Blueprint $table) {
            $table->dropColumn('started');
            $table->dropColumn('finished');
        });
    }
}
