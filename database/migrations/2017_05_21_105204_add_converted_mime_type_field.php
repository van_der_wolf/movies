<?php

use App\Movie;
use App\Movie\MovieFileManager;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddConvertedMimeTypeField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('movies', function (Blueprint $table) {
            $table->string('converted_mime_type');
        });
        $movies = Movie::withTrashed()->get();
        foreach ($movies as $movie) {
            $movie->converted_mime_type = MovieFileManager::getConvertedFileMimeType($movie);
            $movie->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('movies', function (Blueprint $table) {
            $table->dropColumn('converted_mime_type');
        });
    }
}
