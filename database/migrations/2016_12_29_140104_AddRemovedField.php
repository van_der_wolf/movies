<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRemovedField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('movies', function (Blueprint $table) {
            $table->boolean('isRemoved');
        });
        Schema::table('directories', function (Blueprint $table) {
            $table->boolean('isRemoved');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('movies', function (Blueprint $table) {
            $table->dropColumn('isRemoved');
        });
        Schema::table('directories', function (Blueprint $table) {
            $table->dropColumn('isRemoved');
        });
    }
}
