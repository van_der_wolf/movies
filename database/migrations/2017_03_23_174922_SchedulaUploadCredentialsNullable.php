<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SchedulaUploadCredentialsNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('gdrive_schedule_upload', function (Blueprint $table) {
            $table->integer('credentials')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('gdrive_schedule_upload', function (Blueprint $table) {
            $table->integer('credentials')->change();
        });
    }
}
