<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGdriveScheduleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gdrive_schedule_upload', function (Blueprint $table) {
           $table->increments('id');
           $table->integer('credentials');
           $table->boolean('processed');
           $table->integer('file_id');
           $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('gdrive_schedule_upload');
    }
}
