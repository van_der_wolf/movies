<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddParentDirToMovie extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::table('movies', function (Blueprint $table) {
      $table->integer('parentDir');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    Schema::table('movies', function(Blueprint $table) {
      $table->dropColumn('parentDir');
    });
  }
}
