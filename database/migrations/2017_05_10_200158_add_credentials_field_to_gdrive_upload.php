<?php

use App\GdriveMovie;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCredentialsFieldToGdriveUpload extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('gdrive_movie', function (Blueprint $table) {
           $table->integer('credentialsId');
        });
        $mapping = DB::table('gdrive_movie')
            ->join('movies', 'movies.id', '=', 'gdrive_movie.movie_id')
            ->join('gdrive_schedule_upload', 'movies.id', '=', 'gdrive_schedule_upload.file_id')
            ->select('gdrive_movie.id', 'gdrive_schedule_upload.credentials')->get();

        foreach ($mapping as $item) {
            $gdriveMovie = GdriveMovie::find($item->id);
            $gdriveMovie->credentialsId = $item->credentials;
            $gdriveMovie->save();
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('gdrive_movie', function (Blueprint $table) {
            $table->dropColumn('credentialsId');
        });
    }
}
