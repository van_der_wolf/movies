<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTimestampsField extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::table('directories', function(Blueprint $table) {
      $table->timestamps();
    });
    Schema::table('movies', function(Blueprint $table) {
      $table->timestamps();
    });

  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    Schema::table('directories', function(Blueprint $table) {
      $table->dropColumn('created_at');
      $table->dropColumn('updated_at');
    });
    Schema::table('movies', function(Blueprint $table) {
      $table->dropColumn('created_at');
      $table->dropColumn('updated_at');
    });
  }
}
