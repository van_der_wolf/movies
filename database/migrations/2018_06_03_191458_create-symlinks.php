<?php

use App\Movie;
use App\Movie\MovieFileManager;
use App\Movie\MovieRepository;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSymlinks extends Migration
{

    /**
     * @var MovieRepository
     */
    private $movieRepository;

    public function __construct()
    {
        $this->movieRepository = app(MovieRepository::class);
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $movies = $this->movieRepository->getAll();
        $cwd = getcwd();
        chdir(public_path(MovieFileManager::MOVIE_LOCATION));
        foreach ($movies as $movie) {
            if (!file_exists($movie->id))
            {
                symlink(public_path(MovieFileManager::MOVIE_LOCATION) . $movie->fullPath, $movie->id);
            }

        }

        chdir($cwd);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
