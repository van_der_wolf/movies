<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSoftDelete extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::table('movies', function (Blueprint $table) {
      $table->softDeletes();
      $table->dropColumn('isRemoved');
    });
    Schema::table('directories', function (Blueprint $table) {
      $table->softDeletes();
      $table->dropColumn('isRemoved');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    Schema::table('movies', function (Blueprint $table) {
      $table->dropColumn('deleted_at');
      $table->boolean('isRemoved');
    });
    Schema::table('directories', function (Blueprint $table) {
      $table->dropColumn('deleted_at');
      $table->boolean('isRemoved');
    });
  }
}
