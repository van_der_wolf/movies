<?php

use App\Movie;
use App\Movie\MovieFileManager;
use App\Movie\MovieRepository;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMimeType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('movies', function (Blueprint $table) {
            $table->string('mime_type');
        });
        $movies = Movie::withTrashed()->get();
        foreach ($movies as $movie) {
            if (!$movie->dir) {
                $movie->forceDelete();
                continue;
            }
            if ($movie->is_removed) {
                $movie->delete();
            }
            $movie->mime_type = MovieFileManager::getFileMimeType($movie);
            if (!empty($movie->convertedPath)) {
                $movie->convertedPath = MovieFileManager::MOVIE_CONVERTED_LOCATION . '/' . $movie->convertedPath;
            }
            if (in_array($movie->mime_type, MovieFileManager::$webMimeTypes)) {
                $movie->convertedPath = MovieFileManager::MOVIE_LOCATION . $movie->fullPath;
            }
            $movie->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('movies', function (Blueprint $table) {
            $table->dropColumn('mime_type');
        });
    }
}
