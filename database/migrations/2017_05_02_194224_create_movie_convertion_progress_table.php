<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMovieConvertionProgressTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('movie_convertion_progress', function(Blueprint $table) {
           $table->increments('id');
           $table->integer('movieId')->nullable();
           $table->string('percent')->nullable();
           $table->string('eta')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('movie_convertion_progress');
    }
}
