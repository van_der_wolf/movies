<?php

use App\Accounts\Role;
use Illuminate\Database\Seeder;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            'admin' => 'Main role',
            'manage_users' => 'manage user roles and accounts',
        ];

        foreach ($roles as $role => $description) {
            Role::create([
                'name' => $role,
                'description' => $description,
            ]);
        }
    }
}
