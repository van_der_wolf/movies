<?php

use App\Accounts\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      User::create([
        'name' => 'admin',
        'email' => 'dummy_mail@test.loc',
        'password' => bcrypt(1111),
      ]);
    }
}
