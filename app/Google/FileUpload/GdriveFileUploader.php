<?php

namespace App\Google\FileUpload;

use App\GdriveMovie;
use App\Google\GdriveAccountSelector;
use App\Google\GdriveAdapter;
use App\Google\ScheduleUpload\Gdrive\GdriveScheduleUpload;
use App\Google\ScheduleUpload\ScheduleUpload;
use App\Movie;
use App\Movie\MovieFileManager;
use Exception;
use Google_Http_MediaFileUpload;
use Google_Service_Drive_DriveFile;
use Google_Service_Drive_Permission;

class GdriveFileUploader implements FileUploaderInterface
{

    protected $scheduledUpload;

    public function upload(Movie $movie)
    {
        $path = public_path('movies') . $movie->fullPath;
        $adapter = GdriveAdapter::getAdapter();
        $moviefileManager = new MovieFileManager();
        $moviefileManager->setMovie($movie);
        $accountSelector = new GdriveAccountSelector(['FreeSpace' => $moviefileManager->getFileSize()]);
        $credentials = $accountSelector->getAccount();
        var_dump($credentials);

        $client = $adapter->getAuthorizedClient($credentials);
        $service = new \Google_Service_Drive($client);

        $mimeType = mime_content_type($path);
        $file = new Google_Service_Drive_DriveFile();
        $file->title = $movie->name;

        $chunkSizeBytes = 10 * 1024 * 1024;

        // Call the API with the media upload, defer so it doesn't immediately return.
        $client->setDefer(true);
        $request = $service->files->create($file);

        $media = new Google_Http_MediaFileUpload(
            $client,
            $request,
            $mimeType,
            null,
            true,
            $chunkSizeBytes
        );
        $fileSize = filesize($path);
        $media->setFileSize($fileSize);
        $status = false;
        $handle = fopen($path, "rb");
        $counter = 0;
        while (!$status && !feof($handle)) {
            $chunk = fread($handle, $chunkSizeBytes);
            $status = $media->nextChunk($chunk);
            $counter += $chunkSizeBytes;
            print ($counter / $fileSize) * 100 . "\n\r";
        }
        $client->setDefer(false);
        $result = false;
        if ($status != false) {
            $result = $status;
            $fi = new Google_Service_Drive_DriveFile();
            $fi->name = $movie->name;
            $service->files->update($result->id, $fi);
            $permissions = $service->permissions->listPermissions($result->id);
            $newPermission = new Google_Service_Drive_Permission();
            $newPermission->setType('anyone');
            $newPermission->setRole('reader');
            try {
                $service->permissions->create($result->id, $newPermission);
            } catch (Exception $e) {
                print "An error occurred: " . $e->getMessage();
            }
            $gdriveMovie = new GdriveMovie();
            $gdriveMovie->file_id = $result->id;
            $gdriveMovie->movie_id = $movie->id;
            $gdriveMovie->save();

            $movie->gdrive_movie_id = $gdriveMovie->id;
            $movie->save();

            $scheduledUpload->credentials = $credentials->id;
            $scheduledUpload->save();
        }
        fclose($handle);
    }
}