<?php

namespace App\Google\FileUpload;

use App\Google\FileUpload\FileUploaderInterface;
use App\Google\GdriveAccountSelector;
use App\Google\GdriveAdapter;
use App\Google\ScheduleUpload\ScheduleUpload;
use App\Movie;
use Google_Http_MediaFileUpload;
use Google_Service_YouTube;
use Google_Service_YouTube_Video;
use Google_Service_YouTube_VideoSnippet;
use Google_Service_YouTube_VideoStatus;

class YoutubeFileUploader implements FileUploaderInterface {
    public function upload(ScheduleUpload $scheduleUpload) {
        $adapter = GdriveAdapter::getAdapter();
        $accountSelector = new GdriveAccountSelector();
        $credentials = $accountSelector->getAccount();
        $client = $adapter->getAuthorizedClient($credentials);
        $client->setAuthConfig(CLIENT_SECRET_PATH);
        // Create a snippet with title, description, tags and category ID
        // Create an asset resource and set its snippet metadata and type.
        // This example sets the video's title, description, keyword tags, and
        // video category.
        $snippet = new Google_Service_YouTube_VideoSnippet();
        $snippet->setTitle($scheduleUpload->file->name);
        /*$snippet->setDescription("Test description");
        $snippet->setTags(array("tag1", "tag2"));*/

        // Numeric video category. See
        // https://developers.google.com/youtube/v3/docs/videoCategories/list
        $snippet->setCategoryId("22");

        // Set the video's status to "public". Valid statuses are "public",
        // "private" and "unlisted".
        $status = new Google_Service_YouTube_VideoStatus();
        $status->privacyStatus = "unlisted";

        // Associate the snippet and status objects with a new video resource.
        $video = new Google_Service_YouTube_Video();
        $video->setSnippet($snippet);
        $video->setStatus($status);

        // Specify the size of each chunk of data, in bytes. Set a higher value for
        // reliable connection as fewer chunks lead to faster uploads. Set a lower
        // value for better recovery on less reliable connections.
        $chunkSizeBytes = 1 * 1024 * 1024;

        // Setting the defer flag to true tells the client to return a request which can be called
        // with ->execute(); instead of making the API call immediately.
        $client->setDefer(true);
        // Define an object that will be used to make all API requests.
        $youtube = new Google_Service_YouTube($client);
        // Create a request for the API's videos.insert method to create and upload the video.
        $insertRequest = $youtube->videos->insert("status,snippet", $video);

        // Create a MediaFileUpload object for resumable uploads.
        $media = new Google_Http_MediaFileUpload(
            $client,
            $insertRequest,
            'video/*',
            null,
            true,
            $chunkSizeBytes
        );
        $media->setFileSize(filesize(public_path('movies') . $scheduleUpload->file->fullPath));


        // Read the media file and upload it chunk by chunk.
        $status = false;
        $handle = fopen(public_path('movies') . $scheduleUpload->file->fullPath, "rb");
        while (!$status && !feof($handle)) {
            $chunk = fread($handle, $chunkSizeBytes);
            $status = $media->nextChunk($chunk);
        }

        fclose($handle);

        // If you want to make other calls after the file upload, set setDefer back to false
        $client->setDefer(false);
    }
}