<?php

namespace App\Google\FileUpload;

use App\Movie;

interface FileUploaderInterface {
    /**
     * @param Movie $movie
     * @return mixed
     */
    public function upload(Movie $movie);
}