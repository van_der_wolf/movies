<?php

namespace App\Google;

use App\Google\GdriveScheduleUpload;

interface GdriveFileUploaderInterface {
    public function upload(GdriveScheduleUpload $scheduledUpload);
}