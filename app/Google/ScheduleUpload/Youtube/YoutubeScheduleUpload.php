<?php

namespace App\Google\ScheduleUpload\Youtube;

use App\Google\ScheduleUpload\ScheduleUpload;

class YoutubeScheduleUpload extends ScheduleUpload {
    protected $table = 'youtube_schedule_upload';

    public function cred() {
        return $this->hasOne('App\Google\GdriveCredentials', 'id', 'credentials');
    }

    public function file() {
        return $this->hasOne('App\Movie', 'id', 'file_id');
    }

}