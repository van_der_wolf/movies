<?php

namespace App\Google\ScheduleUpload\Youtube;

use App\Core\EloquentRepository;
use App\Google\ScheduleUpload\ScheduleUploadRepositoryInterface;
use App\Movie;

class YoutubeScheduleUploadRepository extends EloquentRepository implements ScheduleUploadRepositoryInterface {

    public function __construct(YoutubeScheduleUpload $model = NULL)
    {
        $this->model = $model;
    }

    /**
     * @inheritdoc
     */
    function scheduleFile(Movie $movie)
    {
        $youtubeSchdeuleUpload = $this->model->where('file_id', $movie->id)->first();
        if (!$youtubeSchdeuleUpload) {
            $youtubeSchdeuleUpload = new YoutubeScheduleUpload();
            $youtubeSchdeuleUpload->file_id = $movie->id;
            return $this->save($youtubeSchdeuleUpload);
        }
        return $youtubeSchdeuleUpload;
    }

    /**
     * @inheritdoc
     */
    function getUnprocessed()
    {
        return $this->model->where('processed', YoutubeScheduleUpload::FILE_UNPROCESSED)->get();
    }

}