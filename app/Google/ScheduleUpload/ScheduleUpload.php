<?php

namespace App\Google\ScheduleUpload;

use Illuminate\Database\Eloquent\Model;

abstract class ScheduleUpload extends Model {
    const FILE_UNPROCESSED = 0;

    const FILE_UPLOADING = 1;

    const FILE_PROCESSED = 2;

    protected $fillable = [
        'credentials',
        'processed',
        'file_id',
    ];
}