<?php

namespace App\Google\ScheduleUpload;

use App\Movie;

interface ScheduleUploadRepositoryInterface {

    /**
     * @param Movie $movie
     * @return mixed
     */
    public function scheduleFile(Movie $movie);

    /**
     * Returns list of unprocessed files
     *
     * @return mixed
     */
    public function getUnprocessed();
}