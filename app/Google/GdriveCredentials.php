<?php

namespace App\Google;

use Illuminate\Database\Eloquent\Model;

class GdriveCredentials extends Model
{
    protected $table = 'gdrive_credentials';

    protected $fillable = [
        'user_id',
        'email',
        'access_token',
        'token_type',
        'expires_in',
        'refresh_token',
        'created'
    ];
}