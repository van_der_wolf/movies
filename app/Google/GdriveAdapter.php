<?php

namespace App\Google;

use Google_Client;
use Google_Service_Drive;
use Google_Service_YouTube;

define('APPLICATION_NAME', 'Drive API PHP Quickstart');
define('CREDENTIALS_PATH', '~/.credentials/drive-php-quickstart.json');
define('CLIENT_SECRET_PATH', __DIR__ . '/client_secret.json');
// If modifying these scopes, delete your previously saved credentials
// at ~/.credentials/drive-php-quickstart.json
define('SCOPES', implode(' ', array(
        Google_Service_Drive::DRIVE,
        Google_Service_YouTube::YOUTUBE
    )
));


class GdriveAdapter
{

    /* const DRIVE_PLUS_ME = 'https://www.googleapis.com/auth/plus.me';*/
    const USER_INFO_EMAIL = 'https://www.googleapis.com/auth/userinfo.email';

    public static function getAdapter()
    {
        return new GdriveAdapter();
    }

    public function getClient()
    {
        $client = new Google_Client();
        $client->setApplicationName(APPLICATION_NAME);
        $client->setScopes([
            SCOPES, /*self::DRIVE_PLUS_ME,*/
            self::USER_INFO_EMAIL
        ]);
        $client->setAuthConfig(CLIENT_SECRET_PATH);
        $client->setAccessType('offline');
        return $client;
    }

    public function getAuthorizedClient($credentials = NULL)
    {
        $client = $this->getClient();
        if (!$credentials) {
            $credentials = GdriveCredentials::where('user_id', auth()->user()->id)->orderBy('created_at', 'desc')->first();
        }

        // @TODO: do something with multiple credentials
        $client->setAccessToken($credentials->toArray());
        return $client;
    }

    /**
     * @return \Google_Service_Drive
     */
    public function getService($credentials = NULL)
    {
        if (!$credentials) {
            $client = $this->getAuthorizedClient();
        } else {
            $client = $this->getAuthorizedClient($credentials);
        }
        $service = new Google_Service_Drive($client);
        return $service;
    }

}