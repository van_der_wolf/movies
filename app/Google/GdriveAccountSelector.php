<?php

namespace App\Google;

use Google_Service_Drive_About;

class GdriveAccountSelector
{

    protected $params = [];

    public function __construct($params = [])
    {
        $this->params = $params;
    }

    public function getAccount($returnService = FALSE)
    {
        $credentials = app(GdriveCredentialsRepository::class)->getAll();
        $adapter = GdriveAdapter::getAdapter();

        foreach ($credentials as $credential) {
            $service = $adapter->getService($credential);
            $about = $service->about->get(['fields' => '*']);
            $valid = TRUE;
            foreach ($this->params as $key => $param) {
                if (method_exists($this, 'has' . $key) && ($valid = $this->{'has' . $key}($about, $param))) {
                    break;
                }
            }
            if ($valid) {
                if ($returnService) {
                    return $service;
                } else {
                    return $credential;
                }
            }
        }
        return FALSE;
    }

    private function hasFreeSpace(Google_Service_Drive_About $about, $size)
    {
        $storageQuota = $about->getStorageQuota();
        $left = $storageQuota['limit'] - $storageQuota['usage'];
        if ($left >= $size) {
            return TRUE;
        }
        return FALSE;
    }

}