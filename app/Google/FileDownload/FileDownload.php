<?php

namespace App\Google\FileDownload;

use App\Google\GdriveAdapter;
use App\Movie;
use Google_Service_Drive;
use GuzzleHttp\Psr7\Request;

class FileDownload
{

    const DOWNLOAD_PORTION = 10485760; // 10 MB

    public function download(Movie $movie)
    {
        //@TODO: adapter and service should be injected to  constructor
        $adapter = GdriveAdapter::getAdapter();
        $client = $adapter->getAuthorizedClient($movie->gdriveMovie->credentials);
        $service = new Google_Service_Drive($client);
        $url = $this->createRequestUri($service, $movie->gdriveMovie->file_id);
        $receivedBytes = self::DOWNLOAD_PORTION;
        $requestedBytes = self::DOWNLOAD_PORTION;
        $previous = 0;
        $file = fopen(public_path($movie->path), 'w+');
        while($receivedBytes === $requestedBytes) {
            $request = $this->createRequest($url, $previous, $previous + $requestedBytes-1);
            $response = $client->execute($request);
            $content = $response->getBody()->getContents();
            fwrite($file, $content);
            $previous += $requestedBytes;
            $receivedBytes = (int) $response->getHeader('Content-Length')[0];
        }
        fclose($file);
    }

    private function createRequest($url, $start, $end) {
        return new Request(
            'GET',
            $url,
            ['content-type' => 'application/json', 'Range' => 'bytes='. $start . '-' . $end],
            ''
        );
    }

    private function createRequestUri($service, $fileId) {
        return $service->files->createRequestUri('files/{fileId}', [
            'fileId' => [
                'location' => 'path',
                'type' => 'string',
                'value' => $fileId,],
            'alt' => ['type' => 'string', 'location' => 'query', 'value' => 'media']
        ]);
    }

}