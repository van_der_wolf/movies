<?php

namespace App\Google;

use App\Core\EloquentRepository;
use App\Movie;

class GdriveScheduleUploadRepository extends EloquentRepository {

  public function __construct(GdriveScheduleUpload $model = NULL) {
    $this->model = $model;
  }

  public function scheduleFile(Movie $movie) {
    $gdriveSchdeuleUpload = $this->model->where('file_id', $movie->id)->first();
    if (!$gdriveSchdeuleUpload) {
      $gdriveSchdeuleUpload = new GdriveScheduleUpload();
      $gdriveSchdeuleUpload->file_id = $movie->id;
      return $this->save($gdriveSchdeuleUpload);
    }
    return $gdriveSchdeuleUpload;
  }

  public function getUnprocessed() {
    return $this->model->where('processed', 0)->get();
  }

}