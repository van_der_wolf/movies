<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Accounts\User;

class AddNewUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:add';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      $name = $this->ask('Username');
      $password = $this->secret('Password');
      $email = $this->ask('Email');
      User::create([
        'name' => $name,
        'email' => $email,
        'password' => bcrypt($password),
      ]);
    }
}
