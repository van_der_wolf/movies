<?php

namespace App\Console\Commands;

use App\Google\FileUpload\GdriveFileUploader;
use App\Google\FileUpload\YoutubeFileUploader;
use App\Google\ScheduleUpload\Gdrive\GdriveScheduleUploadRepository;
use App\Google\ScheduleUpload\Youtube\YoutubeScheduleUploadRepository;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use Log;

class UploadFile extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'file:upload';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      if (Storage::disk('local')->has('.file_upload')) {
        return FALSE;
      } else {
        Storage::disk('local')->put('.file_upload', '1');
        $uploader = new GdriveFileUploader();
        $scheduled = app(GdriveScheduleUploadRepository::class)->getUnprocessed();
        foreach ($scheduled as $item) {
          $this->comment('started ' . $item->file->id);
          $uploader->upload($item);
          $this->comment('finished ' . $item->file->id);
        }

        $youtubeUploader = new YoutubeFileUploader();
        $scheduledYoutubeUploads = app(YoutubeScheduleUploadRepository::class)->getUnprocessed();
        foreach ($scheduledYoutubeUploads as $item) {
            $youtubeUploader->upload($item);
        }
        Storage::disk('local')->delete('.file_upload');
        return TRUE;
      }
    }
}
