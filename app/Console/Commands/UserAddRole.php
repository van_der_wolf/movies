<?php

namespace App\Console\Commands;

use App\Accounts\RoleRepository;
use App\Accounts\UserRepository;
use Illuminate\Console\Command;
use Illuminate\Database\QueryException;

class UserAddRole extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:add-role {user} {role}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $userName = $this->argument('user');
        $roleName = $this->argument('role');
        $user = app(UserRepository::class)->loadUserByName($userName);
        if (empty($user)) {
            $this->error('User not found');
            exit(1);
        }
        $role = app(RoleRepository::class)->getRoleByName($roleName);
        if (empty($role)) {
            $this->error('Role not found');
            exit(1);
        }

        try {
            $user->roles()->attach([$role->id]);
        } catch (QueryException $exception) {

        }

        $user->save();
        return ;
    }
}
