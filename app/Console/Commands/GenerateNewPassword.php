<?php

namespace App\Console\Commands;

use App\Accounts\User;
use App\Accounts\UserRepository;
use Exception;
use Illuminate\Console\Command;

class GenerateNewPassword extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:generate-password {userName} {password}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     * @return mixed
     * @throws Exception
     */
    public function handle()
    {
        if (!$user = User::where('name', $this->argument('userName'))->first())
        {
            throw new Exception("User not found");
        }
        $user->password = bcrypt($this->argument('password'));
        $user->save();
    }
}
