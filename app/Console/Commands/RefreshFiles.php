<?php

namespace App\Console\Commands;

use App\Directory\Directory;
use Illuminate\Console\Command;
use App\Movie;

class RefreshFiles extends Command
{

    protected $dirPath;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'refresh:files';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * @var Movie\MovieFileManagerInterface
     */
    protected $fileManager;

    /**
     * Create a new command instance.
     *
     * @param Movie\MovieFileManagerInterface $fileManager
     * @param Movie $movie
     * @param Directory $directory
     */
    public function __construct(Movie\MovieFileManagerInterface $fileManager, Movie $movie, Directory $directory)
    {
        parent::__construct();
        $this->dirPath = public_path() . '/movies';
        $this->fileManager = $fileManager;
        $this->fileManager->setMovie($movie);
        $this->fileManager->setDirectory($directory);
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->fileManager->refresh();
    }
}
