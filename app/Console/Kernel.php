<?php

namespace App\Console;

use Illuminate\Console\Command;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\AddNewUser::class,
        Commands\RefreshFiles::class,
        Commands\UserAddRole::class,
        Commands\UploadFile::class,
        Commands\GenerateNewPassword::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('refresh:files')
            ->everyFiveMinutes();
        $schedule->command('file:upload')->everyFiveMinutes();
    }
}
