<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'Movie\MovieController@dir')->name('home');
Route::get('/all.m3u', 'Movie\MovieController@playlist');
Route::get('/movie/{dirId}', 'Movie\MovieController@dir');
Route::group([
    'middleware' => ['auth', 'has_role:admin'],
    'prefix' => 'movie',
    'namespace' => 'Movie'
], function () {

    Route::get('edit/{id}', 'MovieController@getEdit')->name('movie.getEdit');
    Route::post('edit/{id}', 'MovieController@postEdit')->name('movie.postEdit');
    Route::post('edit/{id}/delete', 'MovieController@delete')->name('movie.delete')->middleware(['auth', 'has_role:admin']);
    Route::post('edit/{id}/remove', 'MovieController@remove')->name('movie.remove')->middleware(['auth', 'has_role:admin']);
    Route::post('edit/{id}/convert', 'MovieController@convert')->name('movie.convert')->middleware(['auth', 'has_role:admin']);
    Route::post('edit/{id}/download', 'MovieController@download')->name('movie.download')->middleware(['auth', 'has_role:admin']);

});
Auth::routes();
Route::get('/logout', 'Auth\LoginController@logout');
Route::get('/get-csrf-token', 'HomeController@getCSRFtoken');

Route::group(['middleware' => ['auth'], 'prefix' => 'gdrive'], function () {
  Route::get('{fileId}/upload', 'GdriveController@upload')->name('gdrive_upload_file');
});

Route::group([
  'middleware' => ['auth'],
  'prefix' => 'admin',
  'namespace' => 'Admin'
], function () {
  Route::get('/', 'AdminController@index');
  Route::get('role', 'RoleController@listRoles')->name('roles');
  Route::get('role/edit/{role_id}', 'RoleController@getEditRole');
  Route::post('role/edit/{role_id}', 'RoleController@postEditRole');
  Route::get('role/add', 'RoleController@getCreateRole');
  Route::post('role/add', 'RoleController@postSaveRole');
  Route::get('convertion-progress', 'AdminController@listMovieConvertionProgress')->name('convertion_progress');
});
Route::get('/test', 'TestController@index');

Route::group([
  'middleware' => ['auth', 'has_role:admin'],
  'prefix' => 'user'
], function () {
  Route::get('/', 'UserController@listUsers');
  Route::get('edit/{user_id}', 'UserController@getUserEdit')->name('user.edit');
  Route::post('edit/{user_id}', 'UserController@postUserEdit')
    ->name('user.edit');
  Route::get('get-drive-link', 'UserController@getDriveLink');
  Route::post('save-token', 'UserController@saveToken');
});