<?php

namespace App\Http\Middleware;

use Closure;

class HasRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $role)
    {
        if (auth()->guest() || (auth()->check() && ! auth()->user()->hasRoles($role))) {
            abort(403);
        }
        return $next($request);
    }
}
