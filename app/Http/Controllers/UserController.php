<?php

namespace App\Http\Controllers;

use App\Google\GdriveAdapter;
use App\Google\GdriveCredentials;
use Google_Service_Plus;
use Illuminate\Http\Request;
use Input;
use URL;
use Redirect;
use App\Accounts\Role;
use App\Accounts\User;

class UserController extends Controller
{
    private $user;

    private $gdriveCredentials;

    public function __construct(User $user, GdriveCredentials $gdriveCredentials)
    {
        $this->user = $user;
        $this->gdriveCredentials = $gdriveCredentials;
    }

    public function listUsers()
    {
        $users = User::all();

        return view('users.user_list', ['users' => $users]);
    }

    public function getUserEdit($userId)
    {
        $user = $this->user->find($userId);
        if (empty($user)) {
            abort(404);
        }
        $roles = Role::all();
        session()->put('url.intended', URL::previous());
        $gdriveCredentials = $this->gdriveCredentials->where('user_id', auth()->user()->id)->get();
        return view('users.user_edit', ['user' => $user, 'roles' => $roles, 'gdrive_accounts' => $gdriveCredentials]);
    }

    public function postUserEdit( $userId)
    {
        $user = $this->user->find($userId);
        $user->fill(Input::all());
        $user->save();
        $user->roles()->sync((array)Input::get('roles'));

        return Redirect::intended('/');
    }

    public function getDriveLink()
    {
        $client = GdriveAdapter::getAdapter()->getClient();

        return response()->json(['url' => $client->createAuthUrl()]);
    }

    public function saveToken(Request $request)
    {
        $input       = $request->input();
        $adapter     = GdriveAdapter::getAdapter();
        $client      = $adapter->getClient();
        $accessToken = $client->fetchAccessTokenWithAuthCode($input['credentials']);
        if ( ! isset($accessToken['error'])) {
            $client->setAccessToken($accessToken);
            $googlePlus = new Google_Service_Plus($client);
            $userProfile = $googlePlus->people->get('me');
            $email = head($userProfile->getEmails());
            $email = $email->value;
            $accessToken['user_id'] = auth()->user()->id;
            $accessToken['email'] = $email;
            if ($gdriveCredentials = GdriveCredentials::where('email',
                $email)->first()
            ) {
                $gdriveCredentials->fill($accessToken);
                $gdriveCredentials->save();
            } else {
                $gdriveCredentials = new GdriveCredentials($accessToken);
                $gdriveCredentials->save();
            }
        }
    }
}