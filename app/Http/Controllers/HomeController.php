<?php

namespace App\Http\Controllers;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getCSRFtoken() {
        return response()->json(['token' => csrf_token()]);
    }
}
