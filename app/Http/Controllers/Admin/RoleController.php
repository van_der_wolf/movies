<?php

namespace App\Http\Controllers\Admin;

use App\Accounts\Role;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Session;
use URL;
use Redirect;

class RoleController extends Controller {

    public function getCreateRole(Request $request) {
        $role = new Role();
        return view('admin.role_create', ['role' => $role]);
    }

    public function postSaveRole(Request $request, $role_id) {
        $role = new Role();
        $role->name = $request->name;
        $role->description = $request->description;
        $role->save();
        return Redirect::intended('/');
    }

    public function getEditRole(Request $request, $role_id) {
        $role = Role::find($role_id);
        if (empty($role)) {
            abort(404);
        }
        session()->put('url.intended', URL::previous());
        return view('admin.role_edit', ['role' => $role]);
    }

    public function postEditRole(Request $request, $role_id) {
        $input = $request->input();
        $role = Role::find($role_id);
        if (!empty($input['delete'])) {
            $role = Role::find($role_id);
            $role->delete();
            return Redirect::intended('/');
        }
        $role->name = $request->name;
        $role->description = $request->description;
        $role->save();
        return Redirect::intended('/');
    }

    public function listRoles() {
        $roles = Role::all();
        return view('admin.role_list', ['roles' => $roles]);
    }
}