<?php


namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\MovieConvertionProgress;
use App\Preparer;

class AdminController extends Controller {

    /**
     * @var Preparer
     */
    private $preparer;

    public function __construct(Preparer $preparer)
    {
        $this->preparer = $preparer;
    }

    public function index() {
        return redirect('admin/role');
    }

    public function listMovieConvertionProgress() {
        $movieConvertionProgressItems = MovieConvertionProgress::all()->reverse();
        $this->preparer->prepare($movieConvertionProgressItems);
        return view('admin/list_convertion_progress', ['movieConvertionProgressItems' => $movieConvertionProgressItems]);
    }
}