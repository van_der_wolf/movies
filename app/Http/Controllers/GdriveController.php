<?php

namespace App\Http\Controllers;

use App\Movie;
use App\Movie\MovieScheduler;

class GdriveController extends Controller
{
    /**
     * @var Movie
     */
    protected $movie;

    /**
     * GdriveController constructor.
     * @param Movie $movie
     */
    public function __construct(Movie $movie)
    {
        $this->movie = $movie;
    }

    public function upload($localFileId)
    {
        $movie = $this->movie->find($localFileId);

        app(MovieScheduler::class)->scheduleUpload($movie);

        return response()->json(['message' => 'success']);
    }
}
