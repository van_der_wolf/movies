<?php

namespace App\Http\Controllers\Movie;

use App\Directory\Directory;
use App\Directory\DirectoryRepository;
use App\Http\Controllers\Controller;
use App\Movie\MovieFileManagerInterface;
use App\Movie\MovieRepository;
use App\Preparer;
use Illuminate\Http\Request;
use App\Movie;
use App;
use App\Movie\MovieScheduler;
use Illuminate\Routing\UrlGenerator;
use URL;
use Redirect;

class MovieController extends Controller
{

    private $dirPath;

    /**
     * @var UrlGenerator
     */
    private $url;

    /**
     * @var Movie
     */
    private $movie;

    /**
     * @var Directory
     */
    private $directory;

    /**
     * @var MovieRepository
     */
    private $movieRepository;

    /**
     * @var DirectoryRepository
     */
    private $directoryRepository;

    /**
     * @var Preparer
     */
    private $preparer;

    function __construct(Movie $movie, Directory $directory, UrlGenerator $url, MovieRepository $movieRepository, DirectoryRepository $directoryRepository, Preparer $preparer)
    {
        $this->middleware('auth');
        $this->dirPath = public_path('movies');
        $this->movie = $movie;
        $this->directory = $directory;
        $this->url = $url;
        $this->movieRepository = $movieRepository;
        $this->directoryRepository = $directoryRepository;
        $this->preparer = $preparer;
    }

    public function dir($dirId = null)
    {
        if ($dirId) {
            $rootDir = $this->directoryRepository->getDirById($dirId);
        } else {
            $rootDir = $this->directoryRepository->getRoot();
        }

        $movies = $this->movieRepository->getByDirectory($rootDir);
        $preparedMovies = $this->preparer->prepare($movies);
        $dirs = $this->directoryRepository->getChildren($rootDir);
        return view('movie/index', ['dirs' => $dirs, 'movies' => $preparedMovies, 'currentDir' => $rootDir]);
    }

    public function getEdit(Request $request, $id)
    {
        $movie = $this->movieRepository->getById($id);
        session()->put('url.intended', URL::previous());

        return view('movie/edit_movie', ['movie' => $movie]);
    }

    public function postEdit(Request $request, MovieFileManagerInterface $movieFileManager, $id)
    {
        $movie = $this->movieRepository->getById($id);
        $this->movieRepository->saveFromRequest($movie, $request);

        return Redirect::intended('/');
    }

    public function delete(MovieFileManagerInterface $movieFileManager, $id) {
        $movie = $this->movieRepository->getById($id);
        $movieFileManager->deleteFile($movie);
        $movie->delete();

        return response()->json(['success' => true]);
    }

    public function convert($id) {
        $movie = $this->movieRepository->getById($id);
        app(MovieScheduler::class)->scheduleConvert($movie);

        return response()->json(['success' => true]);
    }

    public function download($id) {
        $movie = $this->movieRepository->getById($id);
        app(MovieScheduler::class)->scheduleDownload($movie);
        return response()->json(['success' => true]);
    }
}
