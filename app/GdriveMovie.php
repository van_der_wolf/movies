<?php

namespace App;

use App\Google\GdriveCredentials;

class GdriveMovie extends GoogleMovie
{
    protected $table = 'gdrive_movie';

    protected $fillable = ['file_id', 'movie_id', 'credentialsId'];

    public function movie() {
        return $this->hasOne(Movie::class, 'id', 'movie_id');
    }

    public function credentials() {
        return $this->hasOne(GdriveCredentials::class, 'id', 'credentialsId');
    }
}
