<?php

namespace App\Jobs;

use App\Movie;
use App\Movie\MovieConverter;
use App\Movie\MovieFileManager;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class ConvertJob extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    private $movie;

    /**
     * Create a new job instance.
     *
     * @param Movie $movie
     */
    public function __construct(Movie $movie)
    {
        $this->movie = $movie;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // @TODO: replace constructor with factory, think about injection
        $movieConverter = new MovieConverter();
        $newName = $movieConverter->convert($this->movie);
        $this->movie->convertedPath = MovieFileManager::MOVIE_CONVERTED_LOCATION . '/' .$newName;
        $this->movie->save();
    }
}
