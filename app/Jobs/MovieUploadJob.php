<?php

namespace App\Jobs;

use App\Google\FileUpload\GdriveFileUploader;
use App\Jobs\Job;
use App\Movie;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class MovieUploadJob extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    /**
     * @var Movie
     */
    private $movie;

    /**
     * Create a new job instance.
     *
     * @param Movie $movie
     */
    public function __construct(Movie $movie)
    {
        $this->movie = $movie;
    }

    /**
     * Execute the job.
     *
     * @param GdriveFileUploader $fileUploader
     * @return void
     */
    public function handle(GdriveFileUploader $fileUploader)
    {
        $fileUploader->upload($this->movie);
    }
}
