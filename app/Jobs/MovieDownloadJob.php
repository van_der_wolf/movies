<?php

namespace App\Jobs;

use App\Google\FileDownload\FileDownload;
use App\Movie;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class MovieDownloadJob extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    /**
     * @var Movie
     */
    private $movie;

    /**
     * Create a new job instance.
     *
     * @param Movie $movie
     */
    public function __construct(Movie $movie)
    {
        $this->movie = $movie;
    }

    /**
     * Execute the job.
     *
     * @param FileDownload $fileDownloader
     * @return void
     */
    public function handle(FileDownload $fileDownloader)
    {
        $fileDownloader->download($this->movie);
    }
}
