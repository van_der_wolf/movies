<?php

namespace App\Directory;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Directory extends Model {

  use SoftDeletes;

  /**
   * The attributes that should be mutated to dates.
   *
   * @var array
   */
  protected $dates = ['deleted_at'];

  protected $table = 'directories';
  
  protected $fillable = ['dirName', 'parentDir'];

  public function __get($key) {
    switch ($key) {
      case 'fullPath':
        $fullPath = $this->dirName;
        if (!($parent = $this->parent)) {
          return $fullPath;
        }
        while ($parent->id != 0) {
          $fullPath = $parent->dirName . '/' . $fullPath;
          if (isset($parent->parent)) {
            $parent = $parent->parent;
          } else {
            break;
          }
        }
        return $fullPath . (substr($fullPath, -strlen('/')) === '/' ? '' : '/');
        break;
    }
    return parent::__get($key); // TODO: Change the autogenerated stub
  }

  public function parent() {
    return $this->hasOne(Directory::class, 'id', 'parentDir');
  }

  public function children() {
    return $this->hasMany(Directory::class, 'parentDir', 'id');
  }
}
