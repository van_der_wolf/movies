<?php

namespace App\Directory;

use App\Core\EloquentRepository;

class DirectoryRepository extends EloquentRepository {

    public function __construct(Directory $model = null)
    {
        parent::__construct($model);
    }

    public function getRoot() {
        return $this->model->where('dirName', '/')->first();
    }

    public function getDirById($id) {
        return $this->model->where('id', $id)->first();
    }

    public function getChildren(Directory $directory) {
        return $this->model->where('parentDir', $directory->id)->get();
    }

}