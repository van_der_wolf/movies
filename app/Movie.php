<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Movie extends Model
{

    use SoftDeletes;

    protected $table = 'movies';

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'name',
        'path',
        'parentDir',
        'isWatched',
        'isRemoved',
        'gdrive_movie_id',
        'removedFile',
        'convertedPath',
        'mime_type',
        'converted_mime_type'
    ];

    public function __get($key)
    {
        switch ($key) {
            case 'fullPath':
                $path = $this->dir->fullPath;
                return $path . '/' . $this->path;
        }
        return parent::__get($key);
    }

    public function dir()
    {
        return $this->hasOne('App\Directory\Directory', 'id', 'parentDir');
    }

    public function gdriveMovie() {
        return $this->hasOne('App\GdriveMovie', 'id', 'gdrive_movie_id');
    }

    public function delete()
    {
        parent::delete();
    }

}
