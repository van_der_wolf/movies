<?php

namespace App\Movie;

use App\Jobs\ConvertJob;
use App\Jobs\MovieDownloadJob;
use App\Jobs\MovieUploadJob;
use App\Movie;
use Illuminate\Foundation\Bus\DispatchesJobs;

class MovieScheduler
{

    use DispatchesJobs;

    public function scheduleConvert(Movie $movie)
    {
        $job = (new ConvertJob($movie))->onConnection('database')->onQueue('convert');
        $this->dispatch($job);
    }

    public function scheduleUpload(Movie $movie)
    {
        $job = (new MovieUploadJob($movie))->onConnection('database')->onQueue('upload');
        $this->dispatch($job);
    }

    public function scheduleDownload(Movie $movie)
    {
        $job = (new MovieDownloadJob($movie))->onConnection('database')->onQueue('download');
        $this->dispatch($job);
    }

}