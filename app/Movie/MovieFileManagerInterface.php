<?php

namespace App\Movie;

use App\Directory\Directory;
use App\Movie;

interface MovieFileManagerInterface
{
    public function setMovie(Movie $movie);

    public function setDirectory(Directory $directory);

    public function getFileSize();

    public function deleteFile(Movie $movie);

    public function refresh();
}