<?php

namespace App\Movie;

use App\MovieConvertionProgress;
use App\Preparer;
use Illuminate\Database\Eloquent\Collection;

class MovieConvertionProgressPreparer implements Preparer {

    public function prepare(Collection $data)
    {
        if ($data->getQueueableClass() !== MovieConvertionProgress::class) {
            throw new \Exception('Class should be ' . MovieConvertionProgress::class);
        }

        foreach ($data as $datum) {
            if (isset($datum->started)) {
                $datum->diff = $datum->started->diffInMinutes($datum->finished);
            }

        }

        return $data;
    }

}