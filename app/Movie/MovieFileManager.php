<?php

namespace App\Movie;

use App\Directory\Directory;
use App\Movie;
use Exception;
use Log;
use App;

class MovieFileManager implements MovieFileManagerInterface
{
    const MOVIE_LOCATION = '/movies';
    const MOVIE_CONVERTED_LOCATION = '/converted';

    public static $webMimeTypes = [
        'video/mp4',
        'video/ogg',
        'video/webm'
    ];
    protected $dirPath;

    /**
     * @var Movie
     */
    protected $movie;

    /**
     * @var Directory
     */
    protected $directory;

    public function __construct()
    {
        $this->dirPath = public_path() . '/movies';
    }

    public function setMovie(Movie $movie)
    {
        $this->movie = $movie;
        return $this;
    }

    public function setDirectory(Directory $directory)
    {
        $this->directory = $directory;
        return $this;
    }

    public function getFileSize()
    {
        if ($this->movie) {
            $path = $this->movie->dir->fullPath;
            $path .= $this->movie->path;
            return filesize(public_path('movies') . $path);
        }
        return FALSE;
    }

    public function deleteFile(Movie $movie)
    {
        Log::info('start remove');
        $parentDir = $movie->dir;
        $fullPath = $parentDir->fullPath;
        $path = public_path() . self::MOVIE_LOCATION . $fullPath . $movie->path;
        Log::info('is writable' . is_writable($path));
        Log::info('env ' . env('production'));
        if (is_writable($path) && App::environment('production')) {
            Log::info('trying to remove file ' . $path);
            if (!file_exists($path)) {
                Log::error('File not found ' . $path);
            }
            if (!unlink($path)) {
                Log::error('File cannot be removed ' . $path);
            } else {
                Log::info('File was succesfully removed ' . $path);
            }
        }
    }

    public function refresh()
    {
        $root = $this->createRootDir();
        $this->removeOld();
        $this->scanDirs($root);
    }

    private function removeOld()
    {
        $dirs = $this->directory->all();
        foreach ($dirs as $dir) {
            $files = $this->movie->where('parentDir', $dir->id)->get();
            foreach ($files as $file) {
                if (!file_exists($this->dirPath . $dir->fullPath . '/' . $file->path) && !$file->removedFile) {
                    $file->delete();
                    $cwd = getcwd();
                    chdir(public_path(MovieFileManager::MOVIE_LOCATION));
                    try {
                        unlink($file->id);
                    } catch (Exception $exception) {
                    }
                    chdir($cwd);
                }
            }
            if (!file_exists($this->dirPath . $dir->fullPath)) {
                $dir->delete();
            }
        }
    }

    public function scanDirs($root)
    {
        $dirItems = scandir($this->dirPath . $root->fullPath);
        foreach ($dirItems as $item) {
            echo $item . "\n\r";
            if ($item == '.' || $item == '..' || is_link($this->dirPath . $root->fullPath . '/' . $item)) {
                continue;
            } elseif (is_dir(realpath($this->dirPath . $root->fullPath . '/' . $item))) {
                $dir = $this->createDir($item, $root);
                $this->scanDirs($dir);
            } else {
                $this->createMovie($item, $root);
            }
        }
    }

    public function createRootDir()
    {
        $root = $this->directory->where('dirName', '/')->first();
        if (isset($root)) {
            return $root;
        } else {
            $root = new Directory();
            $root->dirName = '/';
            $root->parentDir = 0;
            $root->save();

            return $root;
        }
    }

    //@TODO: refactor to get file type
    private function createMovie($movieName, $dir)
    {
        $movie = $this->movie->where('path', $movieName)->first();
        if (!isset($movie) && pathinfo($movieName)['extension'] !== 'part') {
            $movie = new Movie();
            $movie->name = $movieName;
            $movie->path = $movieName;
            $movie->parentDir = $dir->id;
            $movie->mime_type = MovieFileManager::getFileMimeType($movie);
            if (in_array($movie->mime_type, MovieFileManager::$webMimeTypes)) {
                $movie->convertedPath = MovieFileManager::MOVIE_LOCATION . $movie->fullPath;
            }
            $movie->save();
            $cwd = getcwd();
            chdir(public_path(MovieFileManager::MOVIE_LOCATION));
            symlink(public_path(MovieFileManager::MOVIE_LOCATION) . $movie->fullPath, $movie->id);
            chdir($cwd);
            return $movie;
        } else {
            return $movie;
        }
    }

    private function createDir($dirName, $parentDir)
    {
        $dir = $this->directory->where('dirName', $dirName)->first();
        if (!isset($dir)) {
            $dir = new Directory();
            $dir->dirName = $dirName;
            $dir->parentDir = $parentDir->id;
            $dir->save();

            return $dir;
        } else {
            return $dir;
        }
    }

    public static function fileExists($movie)
    {
        $parentDir = $movie->dir;
        $fullPath = $parentDir->fullPath;
        $path = public_path() . self::MOVIE_LOCATION . $fullPath . $movie->path;
        return file_exists($path);
    }

    public static function getFileMimeType(Movie $movie)
    {
        $path = public_path() . self::MOVIE_LOCATION . $movie->fullPath;
        return file_exists($path) ? mime_content_type($path) : '';
    }

    public static function getConvertedFileMimeType(Movie $movie): string
    {
        $path = public_path() . $movie->convertedPath;
        return file_exists($path) && $movie->convertedPath ? mime_content_type($path) : '';
    }
}
