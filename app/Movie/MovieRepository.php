<?php

namespace App\Movie;

use App\Core\EloquentRepository;
use App\Directory\Directory;
use Illuminate\Http\Request;
use App\Movie;
use Illuminate\Routing\UrlGenerator;

class MovieRepository extends EloquentRepository {

    /**
     * @var UrlGenerator
     */
    private $url;

    public function __construct(Movie $model = null, UrlGenerator $url)
    {
        parent::__construct($model);
        $this->url = $url;
    }

    public function getByDirectory(Directory $directory) {
        $movies = $this->model->where('parentDir', $directory->id)->get();

        return $movies;
    }

    public function saveFromRequest(Movie $movie, Request $request) {
        $movie->name = $request->name;
        $movie->title = $request->title;
        $movie->isWatched = $request->isWatched;
        $this->save($movie);
        return $movie;
    }

}