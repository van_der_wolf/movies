<?php

namespace App\Movie;

use App\Movie;
use App\Preparer;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Routing\UrlGenerator;

class MoviePreparer implements Preparer {

    /**
     * @var UrlGenerator
     */
    private $urlGenerator;

    public function __construct(UrlGenerator $urlGenerator)
    {
        $this->urlGenerator = $urlGenerator;
    }

    public function prepare(Collection $movies) {
        foreach ($movies as &$movie) {
            $movie->url = $this->urlGenerator->to('/movies/' . $movie->id);
            $movie->class = $this->getClass($movie);
        }
        return $movies;
    }

    public function getClass(Movie $movie) {
        if ($movie->isWatched && $movie->deleted_at) {
            return 'list-group-item-warning';
        } elseif ($movie->isWatched) {
            return 'list-group-item-success';
        } elseif ($movie->deleted_at) {
            return 'list-group-item-danger';
        } else {
            return '';
        }
    }
}