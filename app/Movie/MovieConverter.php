<?php

namespace App\Movie;

use App\Movie;
use App\MovieConvertionProgress;
use Carbon\Carbon;
use Exception;
use Symfony\Component\Process\Process;

class MovieConverter
{

    public function convert(Movie $movie)
    {
        $oldName = public_path($this->createFullPathWithName($movie));
        if (!isset($movie->convertedPath) && mime_content_type($oldName) !== 'video/mp4') {
            $newName = public_path($this->createNewFullName($movie));
            $command = 'HandBrakeCLI --encoder-preset Fast 1080p30 -i "' . $oldName . '" -o "' . $newName . '"';
            $process = new Process($command);
            $process->setTimeout(null);
            $movieConvertionProgress = $this->getOrCreateConvertionProgress($movie);
            $movieConvertionProgress->started = Carbon::now();
            $movieConvertionProgress->save();
            $process->run(function ($type, $buffer) use ($movieConvertionProgress) {
                if (Process::ERR !== $type) {
                    if (preg_match('/^(\r?)Encoding\: task 1 of 1\, (.*?) \% \((.*?) fps\, avg (.*?) fps\, ETA ([\dmhs]+)\)$/', $buffer, $matches)) {
                        $movieConvertionProgress->percent = $matches[2];
                        $movieConvertionProgress->eta = $matches[5];
                        $movieConvertionProgress->save();
                    }
                }
            });
            $exitCode = $process->getExitCode();
            if ($exitCode !== 0) {
                throw new Exception($process->getErrorOutput());
            }
            $movieConvertionProgress->finished = Carbon::now();
            $movieConvertionProgress->save();
        }

        return $this->createNewName($movie);
    }


    private function createNewFullName(Movie $movie)
    {
        return 'converted/' . $this->createNewName($movie);
    }

    private function getOrCreateConvertionProgress(Movie $movie)
    {
        if ($movieConvertionProgress = MovieConvertionProgress::where('movieId', $movie->id)->first()) {
            return $movieConvertionProgress;
        }
        $movieConvertionProgress = new MovieConvertionProgress();
        $movieConvertionProgress->movieId = $movie->id;
        $movieConvertionProgress->save();
        return $movieConvertionProgress;

    }

    private function createNewName(Movie $movie)
    {
        return pathinfo($movie->path)['filename'] . '.mp4';
    }

    /**
     * @param Movie $movie
     * @return string
     * @TODO: movie this method to some more relevant place
     */
    private function createFullPathWithName(Movie $movie)
    {
        return 'movies' . $movie->fullPath;
    }
}