<?php

namespace App\Accounts;

use App\Core\EloquentRepository;

class UserRepository extends EloquentRepository {

    public function __construct(User $model)
    {
        $this->model = $model;
    }

    public function loadUserByName($userName) {
        return $this->model->where('name', $userName)->first();
    }

}