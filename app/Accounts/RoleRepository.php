<?php
namespace App\Accounts;

use App\Core\EloquentRepository;

class RoleRepository extends EloquentRepository
{
    public function __construct(Role $model)
    {
        $this->model = $model;
    }

    public function getRoleList()
    {
        return $this->model->pluck('name', 'id');
    }

    public function getRoleByName($roleName) {
        return $this->model->where('name', $roleName)->first();
    }
}
