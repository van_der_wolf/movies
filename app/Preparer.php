<?php

namespace App;

use Illuminate\Database\Eloquent\Collection;

interface Preparer {

    public function prepare(Collection $data);

}