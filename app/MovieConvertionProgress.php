<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MovieConvertionProgress extends Model
{
    protected $fillable = [
        'movieId',
        'percent',
        'eta',
        'started',
        'finished'
    ];

    protected $dates = [
        'started',
        'finished'
    ];

    public $timestamps = false;

    protected $table = 'movie_convertion_progress';

    public function movie() {
        return $this->hasOne(Movie::class, 'id', 'movieId');
    }
}
