<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

abstract class GoogleMovie extends Model
{
    protected $fillable = ['file_id', 'movie_id'];
}