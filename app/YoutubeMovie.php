<?php

namespace App;

class YoutubeMovie extends GoogleMovie
{
    protected $table = 'youtube_movie';

    public function movie() {
        return $this->hasOne(Movie::class, 'id', 'movie_id');
    }
}
