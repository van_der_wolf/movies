<?php

namespace App\Providers;

use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Movie\MovieController;
use App\Movie\MovieConvertionProgressPreparer;
use App\Movie\MoviePreparer;
use App\Preparer;
use Illuminate\Contracts\Routing\UrlGenerator;
use Illuminate\Support\ServiceProvider;

class PreparerProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->when(MovieController::class)
            ->needs(Preparer::class)
            ->give(function ($app) {
                return new MoviePreparer($this->app->make(UrlGenerator::class));
            });
        $this->app->when(AdminController::class)
            ->needs(Preparer::class)
            ->give(function ($app) {
                return new MovieConvertionProgressPreparer();
            });
    }
}
