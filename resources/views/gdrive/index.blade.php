@extends('layouts.one_column')

@section('content')
    <ul class="list-group">
       @foreach($files as $file)
            <li class="list-group-item">
                <a href="/gdrive/{{ $file->id }}" width="640" height="480" >{{$file->name}}</a>
            </li>
        @endforeach
    </ul>
@endsection