@extends('layouts.one_column')

@section('content')
    <div>
        {{ $file->name }}
        <a href="{{ route('gdrive_share_file', ['fileId' => $file->id]) }}">Share</a>
        @if($file->shared)
            <iframe src="https://docs.google.com/file/d/{{ $file->id }}/preview" width="1454" height="818"></iframe>
        @endif
    </div>
@endsection