@extends('layouts.app')

@section('layout')
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-sm-12 col-md-12">
                {{--<div class="panel panel-default">--}}
                @yield('content')
                {{--</div>--}}
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    @include('scripts')
@endsection