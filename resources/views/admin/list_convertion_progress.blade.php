@extends('layouts.one_column')

@section('content')
    <table class="table table-bordered">
        <thead>
            <tr>
                <td>Name</td>
                <td>Percentage</td>
                <td>Eta</td>
                <td>Time consumed</td>
            </tr>
        </thead>
        <tbody>
            @foreach($movieConvertionProgressItems as $movieConvertionProgressItem)
                <tr>
                    <td>{{ $movieConvertionProgressItem->movie->name }}</td>
                    <td>{{ $movieConvertionProgressItem->percent }}</td>
                    <td>{{ $movieConvertionProgressItem->eta }}</td>
                    <td>{{ $movieConvertionProgressItem->diff }}</td>
                </tr>
                @endforeach
        </tbody>
    </table>
@endsection