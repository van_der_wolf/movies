@extends('layouts.one_column')

@section('content')
    <ul class="list-group">
        @foreach($roles as $role)
            <li class="list-group-item">
                <h4 class="list-group-item-heading">{{ $role->name }}</h4>
                <div class="list-group-item-text">{{ $role->description }}</div>
                <a class="badge" href="{{ url('admin/role/edit', ['id' => $role->id]) }}">Edit</a>
            </li>
        @endforeach
    </ul>
@endsection