@extends('layouts.one_column')

@section('content')
    {!! Form::model($role, ['url' => 'admin/role/add', 'class' => 'form-horizontal']) !!}
    <div class="form-group">
        {!! Form::label('name', 'Name', ['class' => "col-sm-2 control-label"]) !!}
        <div class="col-sm-10">
            {!! Form::text('name', $role->name, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('description', 'Description', ['class' => "col-sm-2 control-label"]) !!}
        <div class="col-sm-10">
            {!! Form::text('description', $role->description, ['class' => 'form-control']) !!}
        </div>
    </div>
    {!! Form::submit('Save', ['class' => 'btn btn-primary pull-right']) !!}
    {!! Form::close() !!}
@endsection