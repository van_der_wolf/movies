@extends('layouts.one_column')

@section('content')
    <ul class="list-group">
        @foreach($users as $user)
            <li class="list-group-item">
                {{ $user->name }}
                <a class="badge" href="{{  url('user/edit', ['id' => $user->id]) }}">Edit</a>
            </li>
        @endforeach
    </ul>
@endsection