@extends('layouts.one_column')

@section('content')
    {!! Form::model($user, ['route' => ['user.edit', $user], 'class' => 'form-horizontal']) !!}
    <div class="form-group">
        {!! Form::label('name', 'Name', ['class' => "col-sm-2 control-label"]) !!}
        <div class="col-sm-10">
            {!! Form::text('name', $user->name, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('email', '', ['class' => "col-sm-2 control-label"]) !!}
        <div class="col-sm-10">
            {!! Form::text('description', $user->description, ['class' => 'form-control']) !!}
        </div>
    </div>
    @if (Auth::check() && Auth::user()->hasRole('admin'))
        <fieldset>
            <legend>Roles</legend>
            @foreach($roles as $role)
                <div class="row">
                    <div class="">

                            <span class="right">
                                {!! Form::checkbox('roles[]', $role->id, $user->hasRole($role->name), ['id' => "role_{$role->id}"]) !!}
                            </span>
                    </div>
                    <div class="small-11 columns">
                        {!! Form::label("role_{$role->id}", $role->name) !!}
                        <p>
                            {{ $role->description }}
                        </p>
                    </div>
                </div>
            @endforeach
        </fieldset>
    @endif
    <fieldset>
        <legend>Google accounts</legend>
        @foreach($gdrive_accounts as $account)
            <div class="form-group">
                <label>{{ $account->email }}</label>
                {!! Form::button('Remove account', ['class' => 'btn btn-default', 'data-account' => $account->id]) !!}
            </div>
        @endforeach
    </fieldset>
    <div class="form-group">
        {!! Form::text('gdrive_token', '',  ['class' => 'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::button('Save token', ['class' => 'btn btn-primary gdrive-save-credentials']) !!}
        {!! Form::button('Connect Google drive account', ['class' => 'btn btn-primary connect-gdrive']) !!}

    </div>
    {!! Form::submit('Save', ['class' => 'btn btn-primary pull-right']) !!}
    {!! Form::close() !!}
@endsection