@extends('layouts.one_column')

@section('content')
    <h3>Directories</h3>
    <ul class="list-group">
        @foreach($dirs as $dir)
            <li class="list-group-item @if($dir->deleted_at) list-group-item-danger @endif">
                <a href="/movie/{{ $dir->id }}">{{ $dir->dirName }}</a>
            </li>
        @endforeach
    </ul>
    <h3>Movies</h3>
    <ul class="list-group movies">
        @foreach($movies as $movie)
            <li class="list-group-item {{ $movie->class }}"
                data-clipboard-support data-clipboard-action="copy"
                data-clipboard-text="{{ $movie->url }}">
                {{ $movie->name }}
                @if(!$movie->deleted_at)
                    <a href="{{ url('movie/edit', ['id' => $movie->id]) }}" class="pull-right">Edit</a>&nbsp;
                @endif
                @if($movie->isRemoved)
                    <span class="badge">Removed</span>&nbsp;
                @endif

                @if(isset($movie->convertedPath))
                    @include('movie/video')
                @elseif(isset($movie->gdriveMovie))
                    @include('movie/gdrive_video')
                @endif

            </li>
        @endforeach
    </ul>
@endsection

@section('analytics')
    @if(App::environment('production'))
        @include('analytics')
    @endif
@endsection

