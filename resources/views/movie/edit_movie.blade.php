@extends('layouts.one_column')

@section('content')
    {!! Form::model($movie, ['route' => ['movie.postEdit', $movie], 'class' => 'form-horizontal']) !!}
    <div class="form-group">
        {!! Form::label('name', 'File name', ['class' => "col-sm-2 control-label"]) !!}
        <div class="col-sm-10">
            {!! Form::text('name', $movie->name, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('isWatched', 'Watched', ['class' => "col-sm-2 control-label"]) !!}
        <div class="col-sm-10">
            {!! Form::checkbox('isWatched', 1, $movie->isWatched, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="dropdown">
        <button class="btn btn-default dropdown-toggle" type="button" id="movieActions" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Actions
            <span class="caret"></span>
        </button>
        <ul class="dropdown-menu" aria-labelledby="movieActions">
            <li><a href="#" class="movie-delete" data-movie-id="{{$movie->id}}">Delete</a></li>
            <li><a href="#" class="movie-convert" data-movie-id="{{$movie->id}}">Convert</a></li>
            @include('movie/form/gdrive_button')
        </ul>
    </div>
    @if(isset($movie->gdriveMovie))
        @include('movie/gdrive_video')
    @endif
    {!! Form::submit('Save', ['class' => 'btn btn-primary pull-right']) !!}
    {!! Form::close() !!}
@endsection

@section('analytics')
    @if(App::environment('production'))
        @include('analytics')
    @endif
@endsection

