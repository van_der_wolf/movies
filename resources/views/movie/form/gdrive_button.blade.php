@if (empty($movie->gdrive_movie_id))
    <li><a href="#" class="gdrive-upload" data-movie-id="{{$movie->id}}">Upload to Gdrive</a></li>
@else
    <li><a href="#" class="gdrive-download" data-movie-id="{{$movie->id}}">Download from Gdrive</a></li>
    <li><a href="#" class="gdrive-remove" data-movie-id="{{$movie->id}}">Remove from Gdrive</a></li>
@endif