<div style="width:100%; height:auto;">
   <video controls id="video-{{$movie->id}}" data-setup='{"controls":true}'
          style="width: 100%; height: auto;position: inherit" {{-- width="350" height="200"--}} class="video-js vjs-default-skin">
       <source src="{{ $movie->convertedPath }}" type="{{$movie->converted_mime_type}}"/>
   </video>
</div>