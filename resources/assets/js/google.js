(function ($) {
    $(document).ready(function () {
        $('.dropdown-toggle').click(function () {
            $('.dropdown-toggle').dropdown();
        });

        function getCSRFToken(callback, callbackArgs) {
            return $.ajax({
                url: '/get-csrf-token'
            }).done(function (data) {
                callback(data.token, callbackArgs)
            });
        }

        function saveToken(csrfToken, credentials) {
            $.ajax({
                url: '/user/save-token',
                method: 'POST',
                data: {
                    'credentials': credentials,
                    '_token': csrfToken
                }
            });
        }

        $('.connect-gdrive').click(function () {
            $.ajax({
                url: '/user/get-drive-link'

            }).done(function (data) {
                my_window = window.open(data.url, "mywindow1", "status=1,width=500,height=500");
            })
        });

        $('.gdrive-save-credentials').click(function () {
            getCSRFToken(saveToken, $('[name="gdrive_token"]').val());
        });

        $('.gdrive-upload').click(function (e) {
            $.ajax({
                url: '/gdrive/' + $(e.target).attr('data-movie-id') + '/upload'
            }).done(function (data) {
                $(e.target).parent('.form-group').append(data.message);
            })
        });

        $('.movie-convert').click(function (e) {
            getCSRFToken(function(csrfToken, movieId) {
                $.ajax({
                    url: '/movie/edit/' + movieId + '/convert',
                    method: 'POST',
                    data: {
                        '_token': csrfToken
                    }
                });
            }, $(e.target).attr('data-movie-id'));
        });

        $('.gdrive-download').click(function (e) {
           getCSRFToken(function (csrfToken, movieId) {
               $.ajax({
                   url: '/movie/edit/' + movieId + '/download',
                   method: 'POST',
                   data: {
                       '_token': csrfToken
                   }
               });
           }, $(e.target).attr('data-movie-id'))
        });

        $('.movie-delete').click(function (e) {
            getCSRFToken(function(csrfToken, movieId) {
                $.ajax({
                    url: '/movie/edit/' + movieId + '/delete',
                    method: 'POST',
                    data: {
                        '_token': csrfToken
                    }
                });
            }, $(e.target).attr('data-movie-id'));
        });
    })
})(jQuery);